// create a worker that uses index_discordChatBot.js
// import worker

// create app in express and use the worker

const express = require('express')
const app = express()
const swaggerJsdoc = require("swagger-jsdoc");
const swaggerUi = require("swagger-ui-express");
const { Worker } = require('worker_threads');

// Utile a test
const { WorkerService } = require('./src/WorkerService.SimplImpl');
const BotModel = require('./src/Bot.model');

var worker;
const personsRouter = require('./src/worker.route.js');
//const { swaggerOptions, APIBaseURL, hostname, port } = require('./config')
//const specs = swaggerJsdoc(swaggerOptions);

app.use(express.static('public'))
app.use(express.json());   
app.use(express.urlencoded({ extended: true })); 

/*app.use(
  "/api-docs",
  swaggerUi.serve,
  swaggerUi.setup(specs, {explorer:true})
);*/
app.use(personsRouter); //service API

// app listen on localhost:3000
app.listen(3000, () => {
    console.log('Server running on http://localhost:3000')
})

app.get('/test', (req,res) => {
  const workerService = WorkerService.getInstance();
  const bot = new BotModel({workerName:"toto",botBrain:"Alice",tocken:"bot_7_1"});
  workerService.runChat(bot); // On run le chat de test
})
