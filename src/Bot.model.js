/*
 *     Class pour l'instanciation de bots
 *     contient toutes les informations necessaire au bon fonctionnement du bot
 *
 */
class BotModel {

  constructor({workerName, idBot, botBrain}){   
    // Test du WorkerName
    if(undefined != workerName) {
        if(this.isSuitableBotName(workerName)) {
            this.workerName = workerName;
        } else {
            throw new Error('not a suitable worker name');
        }
    } else {
      this.workerName = NoName;
    }
    // Test du BotName
    if(undefined != botBrain) {
        if(this.isSuitableBotName(botBrain)) {
            this.botBrain = botBrain;
        } else {
            throw new Error('not a suitable bot name');
        }
    } else {
        this.botBrain = "english.rive";
    }
    // Test du tocken
    if(undefined == idBot) {
      throw new Error('Give a tocken to the BOT');
    } else {
      this.idBot = idBot;
    }
    /*
    console.log("test de l'instanciation")
    console.log(this.workerName);
    console.log(this.botBrain);
    console.log(this.tocken);
    */
  }
  // Not usefull
  /*
    updateMood(newMood){ 
        if(isSuitableMood(newMood)) {
            this.moodBot = newMood;
        } else {
        }
            throw new Error('not a suitable mood');
    }
  */
  // Mise a jour du workerName
  updateWorkerName(newWorkerName){
      if(this.isSuitableWorkerName(newWorkerName)) {
          this.workerName = newWorkerName;
      } else {
          throw new Error('not a suitable worker\'s name');
      }
  }

  // Mise a jour du BotName
  updateBotName(botName){
      if(this.isSuitableBotName(botName)) {
          this.botName = botName;
      } else {
          throw new Error('not a suitable worker\'s name');
      }
  }

  // Serialisation de l'objet
  serialize(){
      return this.dump();
  }

  dump(){
      return {workerName:this.workerName,botBrain:this.botBrain,tocken:this.tocken};
  }

  // Vérification que le botname est bon
  // TODO faire un flitre avec une bdd contenant les nom des brain
  isSuitableBotName(name){
      let response = false;
      if(("string" === typeof name) && (16 > name.length)) {
      response = true;
      } else { 
          console.log(`${name} is not a suitable bot name`);
      }
      return response;
  }

  /*
  function isSuitableMood(mood){
      let response = false;
      if(("string" === typeof name) && (16 > name.length)) {
      response = true;
      } else { 
          console.log(`${name} is not a suitable bot name`);
      }
      return response;
  }
  */
}

module.exports = BotModel;
