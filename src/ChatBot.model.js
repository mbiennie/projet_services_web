// Importations
const { Client,  Events, GatewayIntentBits } = require('discord.js');
const RiveScript = require('rivescript')

/*
  *
  * Class qui va gérer le chatbot
  *
  */
class ChatBotModel {
  constructor(brain,tocken,salon_id) {
    if(undefined == brain) {
      throw new Error('No brain for the bot')
    }
    if(undefined == tocken) {
      throw new Error('No tocken for the bot')
    }
    if(undefined == salon_id) {
      throw new Error('No salon_id for the bot')
    }
    this.brain = brain;
    this.tocken = tocken;
    this.salon_id = salon_id;
  }
  
  run() {

    var bot = new RiveScript();

    // Load a list of files all at once (the best alternative to loadDirectory
    // for the web!)
    bot.loadFile([
      "ressources/brains/"+this.brain,
    ]).then(loading_done).catch(loading_error);

    // All file loading operations are asynchronous, so you need handlers to catch when they've finished. If you use loadDirectory (or loadFile
    // with multiple file names), the success function is called only when ALL
    // the files have finished loading.
    function loading_done() {
      console.log("Bot has finished loading!");

      // Now the replies must be sorted!
      bot.sortReplies();

      // And now we're free to get a reply from the brain!

      // RiveScript remembers user data by their username and can tell
      // multiple users apart.
      let username = "local-user";

      // NOTE: the API has changed in v2.0.0 and returns a Promise now.
      bot.reply(username, "Hello, bot!").then(function(reply) {
        console.log("The bot says: " + reply);
      });
    }

    // It's good to catch errors too!
    function loading_error(error, filename, lineno) {
      console.log("Error when loading files: " + error);
    }

    // Create a new client instance
    const client = new Client({ intents: [GatewayIntentBits.Guilds, GatewayIntentBits.GuildMessages, GatewayIntentBits.MessageContent,GatewayIntentBits.GuildMembers,] });

    const channel = client.channels.cache.get(salon_id);

    client.on('messageCreate', async (message) => {

        if (message.channel.id === salon_id) {

            if(message.author.tag != client.user.tag){


              if(message.mentions.members.first() == client.user.id){
                const channel = client.channels.cache.get(salon_id);
                bot.reply(message.author.tag, message.content).then(function(reply) {
                channel.send(`<@${message.author.id}> : ${reply}`);
              });
            }
          }
            

        }
    }) 

    client.once(Events.ClientReady, readyClient => {
      console.log(`Ready! Logged in as ${readyClient.user.tag}`);
         
        const channel = client.channels.cache.get(salon_id);
        channel.send(`Chat Bot is Ready! Logged in as ${readyClient.user.tag}`);
    });

    // Log in to Discord with your client's token
    client.login(token);
  }
}
module.exports = ChatBotModel;
