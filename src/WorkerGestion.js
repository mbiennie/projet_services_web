const WorkerModel = require("./Worker.model");
const fs = require('fs').promises;
const path = require('path');

// Ajout des lib pour runchat
const { Client,  Events, GatewayIntentBits } = require('discord.js');
const RiveScript = require('rivescript')
const { Worker } = require('worker_threads');


class WorkerGestion {
  // Constructeur de la class
	constructor(){
    this.workerList = []
    this.workerBuff = []
    const dossier = './ressources/database';
	}

	addWorker(nameWorker,botName,botBrain) {
		var trouvee = false;
		for(let i=0;i<this.workerList.length;i++) {
			if(this.workerList[i].getNameWorker()==nameWorker) {
			trouvee = true;
			}
		}
		if(trouvee) {
			console.log("Le nom du worker existe déjà ");
		} else {
			var worker = new WorkerModel(nameWorker,botName,botBrain);
			this.workerList.push(worker);
		}
	}

	async initializeWorkerList() {
    const contenuFichiers = await this.__initializeWorkerList();
    contenuFichiers.forEach(fichier => {
      var worker = new WorkerModel(fichier.workerName, fichier.botName, fichier.botBrain);
      this.workerList.push(worker);
    });
    return this.workerList;
  }

  getWorker(nameWorker) {
    let index = this.workerList.findIndex(item => item.getNameWorker() === nameWorker);
		return this.workerList[index];
  }

  getTockenByWorkerName(nameWorker) {
    let index = this.workerList.findIndex(item => item.getNameWorker() === nameWorker);
		return this.workerList[index].getTocken();
  }
	deleteWorker(nameWorker) {
		let index = this.workerList.findIndex(item => item.getNameWorker() === nameWorker);
		this.workerList[index].deleteWorker();
		this.workerList = this.workerList.filter(item => item.getNameWorker() !== nameWorker);
	}

	setWorkerNameByWorkerName(nameWorker,newNameWorker) {
		var trouvee = false;
		for(let i=0;i<this.workerList.length;i++) {
			if(this.workerList[i].getNameWorker()==newNameWorker) {
        trouvee = true;
			}
		}
		if(trouvee) {
			console.log("Le nom du worker existe déjà ");
		} else {
			let index = this.workerList.findIndex(item => item.getNameWorker() === nameWorker);
			this.workerList[index].updateWorkerName(newNameWorker);
		}
	}

	setBotNameByWorkerName(nameWorker,newBotName) {
		let index = this.workerList.findIndex(item => item.getNameWorker() === nameWorker);
		this.workerList[index].updateBotName(newBotName);
	}

	setBotBrainByWorkerName(nameWorker,newBotBrain) {
		let index = this.workerList.findIndex(item => item.getNameWorker() === nameWorker);
		console.log(index);
		if(index==-1) {
			console.log("Le worker n'existe pas");
		}else{
			this.workerList[index].updateBotBrain(newBotBrain);
		}
	}

	getListOfWorker() {
		return this.workerList;
	}

	addMessage(workerName,speaker, message) {
		let index = this.workerList.findIndex(item => item.getNameWorker() === workerName);
		console.log(workerName);
		console.log(index);
		console.log(this.workerList);
		this.workerList[index].addmessage(speaker, message);
		console.log(this.workerList);
	}

	saveDiscussion(workerName) {
		let index = this.workerList.findIndex(item => item.getNameWorker() === workerName);
		
		this.workerList[index].saveDiscussion();
	}

  /*
    *
    * Cette fonction est ici pour initialiser la class WorkerGestion
    * On va récupérer toutes les informations du dossier ressources/database/
    * Et on va les stocker dans la liste contenuFichiers
    *
    */
  async __initializeWorkerList() {
    const dossier = './ressources/database';
    const contenuFichiers = [];

    try {
        const fichiers = await fs.readdir(dossier);
        console.log(fichiers);
        fichiers.shift();
        // Parcourir tous les fichiers
        for (const fichier of fichiers) {
            // Construire le chemin complet du fichier
            const cheminFichier = path.join(dossier, fichier);

            // Lire le contenu du fichier
            const contenu = await fs.readFile(cheminFichier, 'utf8');
            const worker = JSON.parse(contenu);
            // Stocker le contenu du fichier dans le tableau
            contenuFichiers.push({ nom: fichier, workerName : worker.workername,botName : worker.botName,botBrain : worker.borBrain });
        }
    } catch (err) {
        console.error('Erreur lors de la lecture des fichiers :', err);
    }

    // Retourner le contenu des fichiers
    return contenuFichiers;
}
// Fonction pour run un worker
  runchat(worker) {
    const thread = new Worker('./src/ChatBot.js', {
      workerData: worker.serialize()
    });
  }

}

module.exports = WorkerGestion;
