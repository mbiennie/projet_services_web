const swaggerJsdoc = require('swagger-jsdoc');

// Options de configuration Swagger
const options = {
  swaggerDefinition: {
    openapi: '3.0.0', // Spécification OpenAPI utilisée
    info: {
      title: 'BotsAPI', // Titre de votre API
      version: '1.0.0', // Version de votre API
      description: 'Simple API for Worker Management', // Description de votre API
    },
    servers: [
      {
        url: 'http://localhost:3000/api/v1', // URL de base de votre API
        description: 'Développement',
      },
    ],
  },
  // Les fichiers de routes contenant les commentaires Swagger
  apis: ['./endpoints/workers.route.js'], // Chemin vers vos fichiers de routes
};

// Initialisation de Swagger-jsdoc
const swaggerSpec = swaggerJsdoc(options);

module.exports = swaggerSpec;
