/*
 *     Class pour l'instanciation de bots
 *     contient toutes les informations necessaire au bon fonctionnement du bot
 */
const fs = require("fs");
const path = require('path');
class WorkerModel {
	constructor(workerName,botName,botBrain){
    this.workerName ="";
    this.botBrain ="";
    this.botName = 1;
    this.discussion = [];
    if(undefined != workerName) {
     if(isSuitableWorkerName(workerName)) {
        this.workerName = workerName;
      } else {
        throw new Error('not a suitable worker name');
      }
    }

    // Test du BotName
    if(undefined != botBrain) {
      if(isSuitableBotBrain(botBrain)) {
        this.botBrain = botBrain;
      } else {
        throw new Error('not a suitable bot name');
      }
    } else {
      this.botBrain = "english.rive";
    }

    // Test du tocken
     if(undefined != botName) {
      if(isSuitableBotName(botName)) {
        this.botName = botName;
      } else {
        throw new Error('not a suitable bot name');
      }
    } 
    if(undefined != workerName && undefined != botName && undefined != botBrain) {
      const data = {
        workername: this.workerName,
        botName: this.botName,
        botBrain: this.botBrain
      };

      // Conversion de l'objet en chaîne JSON
      const jsonData = JSON.stringify(data, null, 2);

      // Fonction pour créer et écrire dans un fichier JSON
      fs.writeFile("./ressources/database/"+this.workerName+'.json', jsonData, (err) => {
        if (err) {
    			return console.error('Erreur lors de la création du fichier JSON:', err);
        }
      });
    }else{
      console.log("Worker non enregistrer dans la BDD");
    }
  }

	updateWorkerName(newWorkerName){
        if(isSuitableWorkerName(newWorkerName)) {
            modify_bdd(this.workerName,newWorkerName, this.botName,this.botBrain);
            this.workerName = newWorkerName;
        } else {
           throw new Error('not a suitable worker\'s name');
        }
    }

  async deleteWorker() {
    const fs = require('fs').promises;
    try {
      const cheminFichier = path.resolve(__dirname, './ressources/database', `${this.workerName}.json`);
      
      // Supprimer le fichier
      await fs.unlink(cheminFichier);
    } catch (err) {
      const filePath = "./ressources/database/"+this.workerName+'.json';
      console.error(`Erreur lors de la suppression du fichier ${filePath} :`, err);
    }
  }

  // Mise a jour du BotName
  updateBotName(botName){
    if(isSuitableBotName(botName)) {
      this.botName = botName;
      this.modify_bdd(this.workerName,this.workerName, botName)
    } else {
      throw new Error('not a suitable worker\'s name');
    }
  }

  // Mise a jour du BotName
  updateBotName(botName){
    if(isSuitableBotName(botName)) {
      modify_bdd(this.workerName,this.workerName, botName,this.botBrain);
      this.botName = botName;
    } else {
      throw new Error('not a suitable worker\'s name');
    }
  }

  updateBotBrain(botBrain){
    if(isSuitableBotBrain(botBrain)) {
      modify_bdd(this.workerName,this.workerName, this.botName,botBrain);
      this.botBrain = botBrain;
    } else {
      throw new Error('not a suitable worker\'s name');
    }
  }

  getNameWorker(name) {
    return this.workerName;
  }

  getNameBot() {
    return this.botName;
  }

  getBrain() {
    return this.botBrain;
  }

  // Serialisation de l'objet
  serialize(){
    return JSON.stringify(this.dump())
  }

  dump(){
    return {'workerName':this.workerName,'botName':this.botName,id:this.botName};
  }
    
  addmessage(speaker, message) {
    this.discussion.push(speaker + " : "+message);
  }
  
  saveDiscussion() {
    const pathJSON = `./ressources/discussions/${this.workerName}_discussion.json`;
    const jsonData = JSON.stringify(this.discussion);
    fs.writeFile(pathJSON, jsonData, (err) => {
      if (err) {
    		return console.error('Erreur lors de la création du fichier JSON:', err);
  		}
    });
  }
}

// Vérification que le botname est bon
function isSuitableBotName(name){
    let response = false;
    if("number" === typeof name) {
    response = true;
    } else { 
        console.log(`${name} is not a suitable bot name`);
    }
    return response;
}
function isSuitableBotBrain(name){
    let response = false;
    if(("string" === typeof name) && in_listBot(name)==false) {
    response = true;
    } else { 
        console.log(`${name} is not a suitable bot brain`);
    }
    return response;
}

// Vérification que le botname est bon
function isSuitableWorkerName(name){
  console.log(name)
  let response = false;
  if(("string" === typeof name) && (16 > name.length) && in_listWorker(name)==false) {
    response = true;
  } else { 
    console.log(`${name} is not a suitable bot name`);
  }
  return response;
}

async function modify_bdd(workerName, newWorkerName, newbotname, botBrain) {
    const oldPath = `./ressources/database/${workerName}.json`;
    const newPath = `./ressources/database/${newWorkerName}.json`;

    let fileFound = false;
    var i = 0;
    while (!fileFound) {
    
        try {
            // Vérifiez si le fichier source existe
            await fs.promises.access(oldPath, fs.constants.F_OK);

            // Renommez le fichier
            await fs.promises.rename(oldPath, newPath);
            // Lisez et modifiez les données du fichier
            const data = await fs.promises.readFile(newPath, 'utf8');
            const jsonData = JSON.parse(data);

            // Modifier les champs de l'objet
            jsonData.botName = newbotname;
            jsonData.workername = newWorkerName;
            jsonData.botBrain = botBrain;
            console.log(jsonData);
            // Convertir l'objet modifié en une chaîne JSON
            const modifiedJsonData = JSON.stringify(jsonData, null, 2);

            // Écrire les données modifiées dans le fichier JSON
            await fs.promises.writeFile(newPath, modifiedJsonData);

            fileFound = true; // Indique que le fichier a été trouvé et traité avec succès
            console.log(i);
            
        } catch (err) {
            if (err.code === 'ENOENT') {
                console.error(`Le fichier n'existe pas : ${oldPath}`);
                i=i+1;
                if(i==10)
            {
            	break;
            }
                // Attendez quelques secondes avant de réessayer
                await new Promise(resolve => setTimeout(resolve, 3000));
            } else {
                console.error('Erreur lors du traitement du fichier:', err);
                // Arrêtez le processus si une erreur différente se produit
                throw err;
            }
        }
    }
}
	
function in_listWorker(nameWorker) {
  const repertoire = './ressources/database';
  // Lister les fichiers dans le répertoire
  fs.readdir(repertoire, (err, fichiers) => {
    if (err) {
      console.error('Erreur lors de la lecture du répertoire:', err);
      return;
    }

    fichiers.forEach(fichier => {
      console.log(fichier)
        if(fichier==(nameWorker + ".json")) {
          return true
        }
    });
  });
  return false
}

function in_listBot(nameBot) {   
  console.log("Ligne juste avant l'accès au dossier brains dans ressource");
  const repertoire = './ressources/brains';
  // Lister les fichiers dans le répertoire
  fs.readdir(repertoire, (err, fichiers) => {
    if (err) {
      console.error('Erreur lors de la lecture du répertoire:', err);
      return;
    }
    
    fichiers.forEach(fichier => {
      //console.log(fichier)
      if(fichier==(nameBot + ".json")) {
        //console.log('Le brain a été trouvé !');
        return true
      }
    });
  });
  return false
}


module.exports = WorkerModel;
