const express = require('express')
const router = express.Router()
const controllerBot = require('./controllerBot')

/**
 * @swagger
 * tags:
 *   name: Worker
 *   description: The worker managing API
 * /:
 *   get:
 *     summary: Lists all the worker
 *     tags: [workers]
 *     responses:
 *       200:
 *         description: The list of the workers
 *         content:
 *           application/json:
 *             schema:
 *               type: array
 *               items:
 *                 $ref: '#/components/schemas/workers'
 *       500:
 *         description: Some server error
 *   post:
 *     summary: Create a new worker
 *     tags: [workers]
 *     requestBody:
 *       required: true
 *       content:
 *         application/json:
 *           schema:
 *             $ref: '#/components/schemas/Worker'
 *     responses:
 *       200:
 *         description: The created worker.
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/Worker'
 *       500:
*         description: Some server error
 * /personId/{id}:
 *   get:
 *     summary: Get the person by name
 *     tags: [Workers]
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: string
 *         required: true
 *         description: The worker name
 *     responses:
 *       200:
 *         description: The worker response by name
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/Worker'
 *       404:
 *         description: The person was not found
 *   patch:
 *    summary: Update the worker by the id
 *    tags: [Workers]
 *    parameters:
 *      - in: path
 *        name: id
 *        schema:
 *          type: string
 *        required: true
 *        description: The worker bot
 *    requestBody:
 *      required: true
 *      content:
 *        application/json:
 *          schema:
 *            $ref: '#/components/schemas/Worker'
 *    responses:
 *      200:
 *        description: The updated worker
 *        content:
 *          application/json:
 *            schema:
 *              $ref: '#/components/schemas/Worker'
 *      404:
 *        description: The worker was not found
 *      500:
 *        description: Some error happened
 *   delete:
 *     summary: Remove the worker by name
 *     tags: [Workers]
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: integer
 *         required: true
 *         description: The worker bot
 *     responses:
 *       200:
 *         description: The deleted worker
 *         content:
 *           application/json:
 *             schema:
 *               $ref: '#/components/schemas/Worker'
 *       404:
 *         description: The person was not found
 */


router.get('/', controllerBot.getWorkers)


router.get('/worker/:workerName', controllerBot.getWorkerWithName)
router.get('/worker/:workerName/', controllerBot.getMessageBot)
router.post('/worker/', controllerBot.addWorker)
router.post('/worker/:workerName/saveDiscussion', controllerBot.saveDiscussion)
router.post('/worker/message/', controllerBot.createMessage);
router.patch('/worker/Emmanuel/:nomWorker/', controllerBot.configureBot)
router.get('/worker/Emmanuel/:nomWorker/', controllerBot.getInformationDiscussion)
router.delete('/worker/:workerName', controllerBot.removeWorkerWithName)

module.exports = router
