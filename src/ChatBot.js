/*
 * 
 * On commence par récupérer les donné de l'on nous a trasmis
 *
 */
const ChatBotModel = require('./ChatBot.model');
const WorkerModel = require('./Worker.model.js');
const path = require('path');

const { parentPort, workerData } = require('worker_threads');

if (!workerData) {
  throw new Error('workerData est indéfini');
}

data = JSON.parse(workerData);


bot = new WorkerModel(data.workerName,parseInt(data.botName),data.botBrain); // On instancie le bot a créer

// On récupère le tocken de connection

const { tocken, salon_id } = require('../ressources/tocken/bot_7_'+data.id+'.json');

console.log("test4");
console.log(tocken);
/*
 *  On lance chatbot
 *
 */
chat = new ChatBotModel(bot.botBrain,tocken,salon_id);
chat.run(); // On run le chatbot
