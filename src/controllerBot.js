const WorkerGestion = require("./WorkerGestion");
worker = new WorkerGestion();

async function getWorkerWithName(req, res, next) {
  const workerName = req.params.workerName
  console.log(`workerName: ${workerName}`);
  try {
    //TO DO : réécrire implémentation de getWorkerByName
    //let instance = WorkerService.getInstance();
    await worker.initializeWorkerList();
    const w= await worker.getWorker(workerName);
    res.json(w)
  } catch (error) {
    console.error(`>>> ${error} ${error.stack}`)
    res.status(404).send(`Ressource Not Found`)
  }
}


async function updateWorkerByName(req, res, next){
    try {
        //let instance = WorkerService.getInstance();
        let workerName = req.params.workerName;
        let {botName} = req.body;
        await worker.initializeWorkerList();
        await worker.setBotNameByWorkerName(workerName,botName);
        const w = await worker.getWorker(workerName);
        res.json(w);
    } catch (error) {
        console.error(`>>> ${error} ${error.stack}`)
        res.status(404).send(`Ressource Not Found`)
    }
}

async function getWorkers(req, res, next){
    try {
    	await worker.initializeWorkerList();
        const w = await worker.getListOfWorker();
        
        res.json(w);
    } catch (error) {
        console.error(`>>> ${error} ${error.stack}`)
        res.status(500).send('Internal Server Error')
    }
}

function getMessageBot(req, res, next){
}

async function saveDiscussion(req, res, next){
    const workerName = req.params.workerName;
    try {
        const w = await worker.saveDiscussion(workerName);
        res.json(w);
    } catch (error) {
        console.error(`>>> ${error} ${error.stack}`)
        res.status(500).send('Internal Server Error')
    }
}

async function configureBot(req, res, next){
    const workerName = req.params.workerName;
    const newBotBrain = req.query.nomBrain + ".rive";
    console.log(`workerName: ${workerName} newBotBrain: ${newBotBrain}`);
    try {
        const w = await worker.setBotBrainByWorkerName(workerName,newBotBrain);
        res.json(w);
    } catch (error) {
        console.error(`>>> ${error} ${error.stack}`)
        res.status(500).send('Internal Server Error')
    }
}

function getInformationDiscussion(req, res, next){
}

function removeWorkerWithName(req, res, next){
}

async function addWorker(req, res, next){
    const workerName = req.query.nomWorker;
    const nomBrain = req.query.nomBrain + ".rive";
    const idBot = req.query.idBot;
    console.log(`workerName: ${workerName} nomBrain: ${nomBrain} idBot: ${idBot}`);
    try {
    //let instance = WorkerService.getInstance();
    const idBot = parseInt(req.query.idBot);
    await worker.initializeWorkerList();
    await worker.addWorker(workerName, idBot, nomBrain);
    const w = worker.getWorker(workerName);
    // On run le worker sur discord
    worker.runchat(w);
    res.json(worker);
  } catch (error) {
    console.error(`>>> ${error} ${error.stack}`)
    res.status(500).send('Internal Server Error')
  }
}

async function createMessage(req, res, next){
	
    const workerName = req.params.workerName;
    const speaker = req.query.speaker;
    const contenu = req.query.contenu;
  try {
    console.log(workerName," ",speaker," ",contenu," ");
    //let instance = WorkerService.getInstance();
    var w = await worker.addMessage(workerName,speaker,contenu);
    w = "Message ajouté avec succès"
    res.json(w);
  } catch (error) {
    console.error(`>>> ${error} ${error.stack}`)
    res.status(500).send('Internal Server Error')
  }
}

module.exports = {
    getWorkers,
    updateWorkerByName,
    addWorker,
    getWorkers,
    getWorkerWithName,
    getMessageBot,
    saveDiscussion,
    configureBot,
    createMessage,
    getInformationDiscussion,
    removeWorkerWithName,
}
