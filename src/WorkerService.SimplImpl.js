const fs = require("fs");

const SimplDB = require('simpl.db');
const BotModel = require("./Bot.model");
const WorkerGestion = require("./WorkerGestion");

// Requirement pour le worker
const { Worker } = require('worker_threads');

const dbConfig = {
    autoSave:false, //Whether or not to write data into the JSON file everytime it is updated
    collectionsFolder:"./ressources/database/",
    collectionTimestamps:false, //Whether or not to automatically add the createdAt and updatedAt attributes to every collection entry
    dataFile:"./ressources/database/main_database.json", //The path to the JSON file (from the root of the project) where the main data will be stored
    encryptionKey:null, //The Encryption Key to use in encryption and decryption
    tabSize:1 //The size of the tab in the JSON file (indentation)
}
// fichier de log
const botFilePath = './ressources/database/bot.json'

class WorkerService{
	constructor(data){ 
		this.idCpt = 0; 
	}

	static getInstance(){ // Singleton
		if (!this.instance) {
			this.instance = new WorkerService();
			this.instance.db =  new SimplDB(dbConfig); 
			this.instance.Bot = this.instance.db.createCollection('bot',{$workerName:'Joe'});
		}
		return this.instance;
	}

    getWorkerByName(name){
      let worker;
          const workerRtn = this.BotModel.fetch(u => (u.workerName == name) )
      if(!workerRtn) throw Error(`cannot find person with personId = ${id}`)
      worker = new BotModel(workerRtn);
      return worker.dump();
    }

	addWorker({workerName,idBot,nomBrain}){
    let newBot;
    const worker = new WorkerGestion();
    try{
      newBot = new BotModel({workerName,idBot,nomBrain});
      //TO DO : lancer le BotModel et sauvegarder
			this.runChat(newBot);
			worker.addWorker(workerName, idBot, nomBrain);
		} catch (error){
			throw Error(`cannot create Person ${error} ${error.stack}`);
		}
		return newBot.dump();
	}

  // Quelle update de worker
	updateWorker(moodBot){

    //Does the person exists?
    const tmp = this.BotModel.fetch(u => (u.workerName == name) )
    if(!tmp) throw Error(`cannot find person with personId = ${personId}`)
    const newBot = new BotModel(tmp);
    
    //Let's check with model if the new values fit.
    try{
      newBot.updateWorker(moodBot);
    } catch (e){
      throw Error("cannot update Person");
    }

    //Let's change it in the database
    const temp = this.BotModel.update(	
          worker => {
            worker.workerName = newBot.workerName;
            worker.botName = newBot.botName;
            worker.moodBot = newBot.moodBot;
          },
          filterTarget => (filterTarget.workerName == name )
        );
    if(!temp) throw Error(`cannot update person with personId = ${personId}`)
    this.BotModel.save();
    return newBot.dump();
  }

	getWorkers(){
		let workers = [];
		let worker;
    //TO DO : fetchAll tous les workers
		//let temp = this.BotModel.fetchAll();
		/*temp.forEach(element => {
			//on ne présage pas de l'implémentation du Person
			worker = new BotModel(element);
			worker.push(worker.dump());
		});*/
		return workers;
	}

  runChat(bot) {
    console.log(bot);
    console.log(bot.serialize());
    const worker = new Worker('./src/ChatBot.js', {
      workerData: bot.serialize()
    });
  }
}

module.exports = { WorkerService };
