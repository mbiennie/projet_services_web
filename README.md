# Projet Services web
Ceci est le projet de services web de seconde année de l'ENSSAT réalisé par :
- Biennier Malo
- Bollot Samuel
- Abid-Braga Barara

## Installation
1. cloner le dépot suivant :
```bash
git clone https://gitlab.enssat.fr/mbiennie/projet_services_web.git
cd projet
```
2. Installer les dépendances du projet
```bash
npm init
```
3. Ajouter les tokens
    - Renommer le fichier example_config.json en config.json
    - Ajouter les tokens dans les ""
4. Lancer le service
```bash
npm start
```

## Documentation
GET : /worker/nomWorker => récuperer les infos du worker

POST : /worker/nomWorker=...&nomBrain=...&idBot=... => créer un worker 

POST: worker/nomWorker/saveDiscussion => enregistre les infos de sa conversation actuelle 

POST: /worker/nomWorker/message => envoie d'un message au worker qui s'appelle nomWorker

GET : /worker/nomWorker/message => envoie d'un message par le workers qui s'appelle nomWorker

PATCH: /worker/Emmanuel/nomWorker/nomFichier=... => indication de ce que le bot qui s'appelle nomWorker devra utiliser 

GET: /worker/Emmanuel/nomWorker/ressource=...&dateDebut=...&dateFin=... => réponse HTTP contenant un tableau d'objet (JSON) contenant toute les infos de conversation 

## Backlog
- mise sous forme de class de discordChatBot
- API Rest (routes + workerservices)
- Logs (Dans BotModel)
- Vérification des nom + création d'un nom par défaud (dans BotModel)
- Base de donnée fonctionelle (dans workerservices)

## Répartition des taches
- Samuel
-> API Rest
-> WorkerServices

- Barbara
-> Amélioration de BotModel
-> System de log du bot
-> Gestion de la base de donnée avec les bots

- Malo
-> Gestion du chatbot

## Secrets
Les fichiers de configuration `bot_7_i.json` (0<=i<=3) sont de la forme {`tocken`: ..., `salon_id`: ...} avec `tocken` le tocken du bot i.
